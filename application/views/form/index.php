<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $title . ' ' . $event_name; ?></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" method="post" action="<?= base_url($action); ?>">
        <input type="hidden" name="id" value="<?= @$this->uri->segment(3); ?>" name="event_id">
        <div class="box-body">
            <div class="form-group">
                <label for="full_name" class="col-sm-2 control-label">Nama Lengkap</label>

                <div class="col-sm-10">
                    <input type="text" name="full_name" required class="form-control" id="full_name" placeholder="Nama Lengkap" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>

                <div class="col-sm-10">
                    <input name="email" type="email" required class="form-control" id="email" placeholder="Email" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="whatsapp_number" class="col-sm-2 control-label">No. Whatsapp</label>

                <div class="col-sm-10">
                    <input name="whatsapp_number" type="text" required class="form-control" id="whatsapp_number" placeholder="No. Whatsapp" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="institute" class="col-sm-2 control-label">Asal Kota/Kabupaten</label>

                <div class="col-sm-10">
                    <input name="institute" type="text" required class="form-control" id="institute" placeholder="Asal Kota/Kabupaten" value="">
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Simpan</button>
        </div>
        <!-- /.box-footer -->
    </form>
</div>
<h4>Data Registrasi <?= @$event_name; ?></h4>
<table border="1" cellpadding="8">
    <tr>
        <th>No</th>
        <th>Nama Lengkap</th>
        <th>Email</th>
        <th>No. Whatsapp</th>
        <th>Asal Kota/Kabupaten</th>
        <th>Tanggal Registrasi</th>
    </tr>
    <?php
    if (!empty($registration)) { // Jika data pada database tidak sama dengan empty (alias ada datanya)
        foreach ($registration as $reg) { // Lakukan looping pada variabel siswa dari controller
            echo "<tr>";
            echo "<td>" . $no++ . "</td>";
            echo "<td>" . $reg->full_name . "</td>";
            echo "<td>" . $reg->email . "</td>";
            echo "<td>" . $reg->whatsapp_number . "</td>";
            echo "<td>" . $reg->institute . "</td>";
            echo "<td>" . $reg->created_at . "</td>";
            echo "</tr>";
        }
    } else { // Jika data tidak ada
        echo "<tr><td colspan='6'>Data tidak ada</td></tr>";
    }
    ?>
</table>
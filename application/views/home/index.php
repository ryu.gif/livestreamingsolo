<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Daftar <?= $title; ?></h3>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th style="width: 5%;">No</th>
                    <th style="width: 20%;">Nama Event</th>
                    <th style="width: 25%;">Zoom URL</th>
                    <th style="width: 25%;">Periode Event</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($events as $event) { ?>
                    <tr>
                        <td style="width: 5%;"><?= $no++; ?>.</td>
                        <td style="width: 20%;"><?= $event->event_name; ?></td>
                        <td style="width: 25%;"><?= $event->redirect_url; ?></td>
                        <td style="width: 25%;"><?= tgl_indo($event->start) . ' - ' . tgl_indo($event->end); ?></td>
                        <td>
                            <a href="<?= base_url($event->event_slug); ?>" target="_blank" type="button" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                            <a href="<?= base_url('home/export/' . encode_id($event->id)); ?>" type="button" class="btn btn-success"><i class="fa fa-print"></i></a>
                            <a href="<?= base_url('home/create/' . encode_id($event->id)); ?>" type="button" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                            <button onclick="delete_event('<?= encode_id($event->id); ?>')" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <!-- /.table -->
        <!-- /.mail-box-messages -->
    </div>
    <!-- /.box-body -->
</div>
<!-- /. box -->
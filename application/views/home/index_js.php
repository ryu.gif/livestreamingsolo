<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });

    function delete_event(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Event yang dipilih akan dihapus",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('home/delete_event') ?>',
                    dataType: 'JSON',
                    data: {
                        id: id
                    },
                    success: function(res) {
                        window.location.reload();
                    },
                });
                return false
            }
        })
    }
</script>
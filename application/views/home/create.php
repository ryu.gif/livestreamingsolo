<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $title; ?></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" method="post" action="<?= base_url($action); ?>">
        <input type="hidden" name="id" value="<?= @$this->uri->segment(3); ?>" name="event_id">
        <div class="box-body">
            <div class="form-group">
                <label for="event_name" class="col-sm-2 control-label">Nama Event</label>

                <div class="col-sm-10">
                    <input type="text" name="event_name" required class="form-control" id="event_name" placeholder="Nama Event" value="<?= @$event->event_name; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="redirect_url" class="col-sm-2 control-label">Zoom URL</label>

                <div class="col-sm-10">
                    <input name="redirect_url" type="text" required class="form-control" id="redirect_url" placeholder="Zoom Url" value="<?= @$event->redirect_url; ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="event_date" class="col-sm-2 control-label">Tanggal</label>

                <div class="col-sm-10">
                    <input name="event_date" type="text" required class="form-control" id="event_date" placeholder="Tanggal" value="
                    <?php if (isset($event->start) && isset($event->end)) echo date("m/d/Y", strtotime($event->start)) . ' - ' . date("m/d/Y", strtotime($event->end)); ?>">
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Simpan</button>
        </div>
        <!-- /.box-footer -->
    </form>
</div>
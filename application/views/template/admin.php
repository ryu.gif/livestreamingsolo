<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= base_url(); ?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="<?= base_url(); ?>plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>plugins/fullcalendar/fullcalendar.print.css" media="print">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url(); ?>dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?= base_url(); ?>dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url(); ?>plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="<?= base_url(); ?>plugins/daterangepicker/daterangepicker.css">
    <title>Livestreamingsolo<?php isset($title) ? print(' - ' . $title) : null; ?></title>
    <style>
        .swal2-popup {
            font-size: 1.6rem !important;
        }
    </style>
    <link rel="icon" href="<?= base_url(); ?>landing/live.png">
</head>

<body class="skin-blue sidebar-mini">
    <div class="content-wrapper" style="min-height: 921px; margin-left: 0px;padding: 4% 15%;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Livestreamingsolo.com</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-3">
                    <a href="<?= base_url('home/create'); ?>" class="btn btn-primary btn-block margin-bottom">Buat Event Baru</a>
                    <br>
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Menu</h3>

                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked">
                                <li class=""><a href="<?= base_url('home'); ?>"><i class="fa fa-circle-o text-primary"></i> Semua Event</a></li>
                                <li class=""><a href="<?= base_url('home/index/upcoming'); ?>"><i class="fa fa-circle-o text-primary"></i> Event yang akan datang</a></li>
                                <li class=""><a href="<?= base_url('home/index/progress'); ?>"><i class="fa fa-circle-o text-primary"></i> Event yang sedang berlangsung</a></li>
                                <li class=""><a href="<?= base_url('home/index/done'); ?>"><i class="fa fa-circle-o text-primary"></i> Event selesai</a></li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-9">
                    <?= $this->session->flashdata('message'); ?>
                    <?php
                    if (isset($isi)) {
                        $this->load->view($isi);
                    } else {
                        $this->load->view('content_default');
                    }
                    ?>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <!-- jQuery 2.2.3 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="<?= base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?= base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?= base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url(); ?>plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url(); ?>dist/js/app.min.js"></script>
    <script src="<?= base_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap.min.js"></script>

    <?php if (isset($extra_js)) {
        echo $extra_js;
    } ?>
</body>

</html>
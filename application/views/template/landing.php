<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- SEO Meta Tags -->
    <meta name="description" content="Website livestreamingsolo.com created by Ramadhan Wahyu">
    <meta name="author" content="Ramadhan Wahyu">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="livestreamingsolo" /> <!-- website name -->
    <meta property="og:site" content="livestreamingsolo.com" /> <!-- website link -->
    <meta property="og:title" content="livestreamingsolo" /> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="Website livestreamingsolo.com created by Ramadhan Wahyu" />
    <!-- description shown in the actual shared post -->
    <meta property="og:type" content="website" />

    <!-- Website Title -->
    <title>Livestreamingsolo.com</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<?= base_url('landing/'); ?>css/styles.css" rel="stylesheet">
    <style>
        .row {
            display: flex;
            flex-wrap: wrap;
            padding: 0 4px;
        }

        /* Create four equal columns that sits next to each other */
        .column {
            flex: 25%;
            max-width: 33.3%;
            padding: 0 4px;
        }

        .column img {
            margin-top: 8px;
            vertical-align: middle;
            width: 100%;
            filter: grayscale(1) brightness(0.5);
            border-radius: 5px;
            cursor: pointer;
            transition: 0.3s linear;
        }

        .column img:hover {
            filter: grayscale(0);
        }

        @media screen and (max-width: 800px) {
            .column {
                flex: 50%;
                max-width: 50%;
            }
        }

        /* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 600px) {
            .column {
                flex: 100%;
                max-width: 100%;
            }

            .column img {
                filter: grayscale(0) brightness(1);
            }
        }
    </style>
    <link rel="icon" href="<?= base_url(); ?>landing/live.png">
</head>

<body data-spy="scroll" data-target=".fixed-top">

    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5">
                        <div class="text-container">
                            <h1>Livestreamingsolo.com</h1>
                            <p class="p-large">Virtual Set, Webinar ,live Event ,live Streaming
                                Digital Meng, Video Conference ,live Facebook ,live Instagram ,live Youtube
                                Live Webcast, Broadcasng</p>
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <svg class="header-frame" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 1920 310">
        <defs>
            <style>
                .cls-1 {
                    fill: #5f4def;
                }
            </style>
        </defs>
        <title>header-frame</title>
        <path class="cls-1" d="M0,283.054c22.75,12.98,53.1,15.2,70.635,14.808,92.115-2.077,238.3-79.9,354.895-79.938,59.97-.019,106.17,18.059,141.58,34,47.778,21.511,47.778,21.511,90,38.938,28.418,11.731,85.344,26.169,152.992,17.971,68.127-8.255,115.933-34.963,166.492-67.393,37.467-24.032,148.6-112.008,171.753-127.963,27.951-19.26,87.771-81.155,180.71-89.341,72.016-6.343,105.479,12.388,157.434,35.467,69.73,30.976,168.93,92.28,256.514,89.405,100.992-3.315,140.276-41.7,177-64.9V0.24H0V283.054Z" />
    </svg>
    <!-- end of header -->

    <!-- Description -->
    <div class="cards-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h2-heading">KENAPA MEMILIH KAMI?</h2>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="card border-0 shadow rounded-xs">
                    <div class="card-body">
                        <img src="<?= base_url('landing/'); ?>illust/19199626.jpg" alt="" class="img-fluid">
                        <h4 class="mt-4 mb-3">PERALATAN MUTAKHIR</h4>
                        <p class="p-2">Kami menggunakan peralatan berstandar internasional seperti Datavideo,
                            Blackmagic Design, Sony dengan resolusi Full HD sehingga output streaming yang diterima
                            penonton terlihat tajam dan jelas.</p>
                    </div>
                </div>
                <div class="card border-0 shadow rounded-xs">
                    <div class="card-body">
                        <img src="<?= base_url('landing/'); ?>illust/crew.jpg" alt="" class="img-fluid" style="height: 200px;">
                        <h4 class="mt-4 mb-3">KRU BERPENGALAMAN</h4>
                        <p class="p-2">Dengan pengalaman tim kami yang rata-rata jebolan TV Swasta, kami percaya
                            diri menerima tantangan demi tantangan klien. Mengerjakan produksi Broadcasting sudah
                            menjadi keseharian kami baik TV Lokal maupun Nasional.</p>
                    </div>
                </div>
                <div class="card border-0 shadow rounded-xs">
                    <div class="card-body">
                        <img src="<?= base_url('landing/'); ?>illust/speed.jpg" class="img-fluid" style="height: 200px;">
                        <h4 class="mt-4 mb-3">INTERNET PROVIDER</h4>
                        <p class="p-2">Kunci kesuksesan Live Streaming adalah kemampuan internet. Kami telah
                            mengerjakan banyak even khususnya olahraga, mulai dari Aceh sampai Papua sehingga kami
                            mempunyai banyak rekanan Internet Provider hampir di seluruh pelosok Indonesia.</p>
                    </div>
                </div>
                <div class="col-lg-12">



                    <p>Telah lama kami memulai kerjasama dengan Provider Internet di Indonesia seperti Telkom. Astine,
                        Biznet dll, yang menyediakan dedicated bandwidth internet hampir diseluruh kota di Indonesia
                        mulai 20-100mbps. Dan untuk menjangkau daerah yang tidak tersedia Internet Provider, kami juga
                        menyediakan teknologi mutakhir 4G Bonding untuk memperkuat bandwidth upload saat streaming.</p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-1 -->
    <!-- end of description -->


    <!-- Features -->
    <div id="features" class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h2-heading text-center">Testimoni</h2>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div id="details" class="basic-1 p-5 mb-5 features">
                <div class="container-fluid">
                    <div class="row">
                        <div class="column">
                            <img src="<?= base_url('landing/'); ?>uploads/1.jpeg" />
                            <img src="<?= base_url('landing/'); ?>uploads/2.jpeg" />
                            <img src="<?= base_url('landing/'); ?>uploads/3.jpeg" />
                            <img src="<?= base_url('landing/'); ?>uploads/4.jpeg" />
                            <img src="<?= base_url('landing/'); ?>uploads/5.jpeg" />
                        </div>
                        <div class="column">
                            <img src="<?= base_url('landing/'); ?>uploads/6.jpeg">
                            <img src="<?= base_url('landing/'); ?>uploads/7.jpeg">
                            <img src="<?= base_url('landing/'); ?>uploads/8.jpeg">
                            <img src="<?= base_url('landing/'); ?>uploads/9.jpeg">
                            <img src="<?= base_url('landing/'); ?>uploads/15.jpeg">
                        </div>
                        <div class="column">
                            <img src="<?= base_url('landing/'); ?>uploads/10.jpeg" />
                            <img src="<?= base_url('landing/'); ?>uploads/11.jpeg" />
                            <img src="<?= base_url('landing/'); ?>uploads/12.jpeg" />
                            <img src="<?= base_url('landing/'); ?>uploads/13.jpeg" />
                            <img src="<?= base_url('landing/'); ?>uploads/14.jpeg" />
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end of container -->
    </div> <!-- end of tabs -->
    <!-- end of features -->


    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer-col first">
                        <h4>Tentang Kami</h4>
                        <p class="p-small">Virtual Set, Webinar ,live Event ,live Streaming Digital Meng, Video
                            Conference ,live Facebook ,live Instagram ,live Youtube Live Webcast, Broadcasng</p>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-6">
                    <div class="footer-col last">
                        <h4>Contact</h4>
                        <ul class="list-unstyled li-space-lg p-small">
                            <li class="media">
                                <i class="fas fa-map-marker-alt"></i>
                                <div class="media-body">Kantor. Jl. Manggar Raya No 3 RT I/II Tambak Grogol Sukoharjo.
                                    (Belakang Thepark)</div>
                            </li>
                            <li class="media">
                                <i class="fas fa-phone"></i>
                                <div class="media-body">
                                    <a class="white" href="#">085728330334 (kaleb)</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of footer -->
    <!-- end of footer -->


    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">Copyright © <?= date('Y'); ?></p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright -->
    <!-- end of copyright -->


    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="<?= base_url('landing/'); ?>js/scripts.js"></script> <!-- Custom scripts -->
</body>

</html>
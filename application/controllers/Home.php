<?php


defined('BASEPATH') or exit('No direct script access allowed');
class Home extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in') == FALSE) {
            redirect(base_url('auth'), 'refresh');
        }
    }

    public function index($where = null)
    {
        $title = 'Semua Event';
        switch ($where) {
            case 'upcoming':
                $option = [
                    'start >' => date('Y-m-d'),
                    'end >' => date('Y-m-d'),
                ];
                $title = 'Event Yang Akan Datang';
                break;
            case 'progress':
                $option = [
                    'start <=' => date('Y-m-d'),
                    'end >=' => date('Y-m-d'),
                ];
                $title = 'Event Sedang Berlangsung';
                break;
            case 'done':
                $option = [
                    'start <' => date('Y-m-d'),
                    'end <' => date('Y-m-d'),
                ];
                $title = 'Event Selesai';
                break;
        }

        if ($where !== null) {
            $this->db->where($option);
        }
        $events = $this->db->where(['deleted_at' => null])->get('event')->result();


        $data = [
            'isi' => 'home/index',
            'extra_js' => $this->load->view('home/index_js', NULL, TRUE),
            'events' => $events,
            'no' => 1,
            'title' => $title,
            'menu_active' => 'all'
        ];
        $this->load->view('template/admin', $data, FALSE);
    }

    public function delete_event()
    {
        $id = decode_id($this->input->post('id'));
        $this->db->where('id', $id);
        $this->db->update('event', [
            'deleted_at' => date('Y-m-d H:i:s')
        ]);

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Event berhasil dihapus.</div>');
        header('Content-Type: application/json');
        echo json_encode([
            'status' => 'success',
            'message' => 'delete successfull'
        ]);
    }

    public function create($id = null)
    {
        if ($id !== null) {
            $data = [
                'title' => 'Ubah Event',
                'isi' => 'home/create',
                'extra_js' => $this->load->view('home/create_js', NULL, TRUE),
                'event' => $this->db->where([
                    'id' => decode_id($id),
                    'deleted_at' => null
                ])->get('event')->row(),
                'action' => 'home/edit_event'
            ];
        } else {
            $data = [
                'title' => 'Event Baru',
                'isi' => 'home/create',
                'action' => 'home/create_event',
                'extra_js' => $this->load->view('home/create_js', NULL, TRUE)
            ];
        }
        $this->load->view('template/admin', $data, FALSE);
    }

    public function create_event()
    {
        $cek = $this->db->select('event_name')->where(['event_name' => $this->input->post('event_name'), 'deleted_at' => null])->get('event', 1);
        if ($cek->num_rows() == 1) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Nama event sudah ada.</div>');
            redirect('home/create');
        }

        $event_date = $this->input->post('event_date');
        $event_date = explode(' - ', $event_date);
        $data = [
            'event_name' => $this->input->post('event_name'),
            'redirect_url' => $this->input->post('redirect_url'),
            'start' => date("Y-m-d", strtotime($event_date[0])),
            'end' => date("Y-m-d", strtotime($event_date[1])),
            'event_slug' => self::slugify($this->input->post('event_name')),
            'created_at' => date('Y-m-d H:i:s'),
            'edited_at' => date('Y-m-d H:i:s'),
        ];

        $this->db->insert('event', $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Event berhasil dibuat.</div>');
        redirect('home/create');
    }

    public function edit_event()
    {
        $id = $this->input->post('id');
        $event_date = $this->input->post('event_date');
        $event_date = explode(' - ', $event_date);
        $data = [
            'event_name' => $this->input->post('event_name'),
            'redirect_url' => $this->input->post('redirect_url'),
            'start' => date("Y-m-d", strtotime($event_date[0])),
            'end' => date("Y-m-d", strtotime($event_date[1])),
            'event_slug' => self::slugify($this->input->post('event_name')),
            'created_at' => date('Y-m-d H:i:s'),
            'edited_at' => date('Y-m-d H:i:s'),
        ];

        $this->db->where('id', decode_id($id));
        $this->db->update('event', $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Event berhasil diubah.</div>');
        redirect('home/create/' . $id);
    }

    public function export($id)
    {
        $id = decode_id($id);
        $event_name = $this->db->select('event_name')->where([
            'deleted_at' => null,
            'id' => $id
        ])->get('event', 1)->row()->event_name;
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Data Registrasi $event_name.xls");


        $data = [
            'no' => 1,
            'event_name' => $event_name,
            'registration' => $this->db->where([
                'event_id' => $id
            ])->get('registration')->result()
        ];
        $this->load->view('home/export', $data);
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}

/* End of file Home.php */

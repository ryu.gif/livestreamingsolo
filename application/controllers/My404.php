<?php


defined('BASEPATH') or exit('No direct script access allowed');

class My404 extends CI_Controller
{

    public function index()
    {
        $data = [
            'title' => 'Not Found',
            'isi' => 'error/not_found'
        ];
        $this->load->view('template/form', $data, FALSE);
    }
}

/* End of file Error.php */

<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    private const token = 'dcrxUtn0fnVwY9Nzv0CX';

    public function index()
    {
        $this->load->view('template/auth', NULL, FALSE);
    }

    // public function register()
    // {
    //     $email = 'guest@app.com';
    //     $pwd = 'admin';
    //     $key = self::token;
    //     $pwd_peppered = hash_hmac("sha256", $pwd, $key);
    //     $pwd_hashed = password_hash($pwd_peppered, PASSWORD_DEFAULT);
    //     $data = [
    //         'email' => $email,
    //         'password' => $pwd_hashed,
    //     ];
    //     $data = $this->security->xss_clean($data);
    //     $query = $this->db->insert('auth', $data);
    //     if ($this->db->affected_rows()) {
    //         return true;
    //     }
    //     return null;
    // }

    public function validate()
    {
        $key = self::token;
        $pwd = $this->security->xss_clean($this->input->post('password'));
        $email = $this->security->xss_clean($this->input->post('email'));

        if (@$pwd || @$email) {
            $pwd_peppered = hash_hmac("sha256", $pwd, $key);

            $row = $this->db->where(['email' => $email, 'deleted_at' => null])->get('auth', 1);
            if ($row->num_rows() > 0) {
                $pwd_hashed = $row->row()->password;

                if (password_verify($pwd_peppered, $pwd_hashed)) {
                    $query = $this->db
                        ->select('a.id, a.name')
                        ->where([
                            'a.email' => $email,
                            'a.deleted_at' => null,
                        ])
                        ->get('auth a', 1)
                        ->row();

                    $sesdata = array(
                        'logged_in'     => TRUE,
                        'email' => $email,
                        'name' => $query->name,
                        'id' => encode_id($query->id),
                        'session_id' => random_string('alnum', 16)
                    );
                    $this->session->set_userdata($sesdata);
                    redirect(base_url('home'), 'refresh');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email/password salah.</div>');
                    redirect(base_url('auth'), 'refresh');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email/password salah.</div>');
                redirect(base_url('auth'), 'refresh');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email/password tidak boleh kosong.</div>');
            redirect(base_url('auth'), 'refresh');
        }
    }
}

/* End of file Auth.php */

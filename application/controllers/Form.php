<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Form extends CI_Controller
{

    public function register($slug = null)
    {
        if ($slug === null) {
            $data = [
                'title' => 'Not Found',
                'isi' => 'error/not_found'
            ];
            $this->load->view('template/form', $data, FALSE);
        }

        $q = $this->db->where([
            'deleted_at' => null,
            'event_slug' => $slug
        ])->get('event', 1);

        if ($q->num_rows() > 0) {
            $event = $q->row();
            $data = [
                'isi' => 'form/index',
                'title' => 'Registrasi Event ',
                'action' => 'form/registration/' . encode_id($event->id),
                'event_name' => $event->event_name
            ];
            $this->load->view('template/form', $data, FALSE);
        }
    }

    public function registration($id)
    {
        $id = decode_id($id);
        $redirect_url = $this->db->select('redirect_url')->where([
            'deleted_at' => null,
            'id' => $id
        ])->get('event', 1)->row()->redirect_url;

        $data = [
            'full_name' => $this->input->post('full_name'),
            'email' => $this->input->post('email'),
            'institute' => $this->input->post('institute'),
            'whatsapp_number' => $this->input->post('whatsapp_number'),
            'created_at' => date('Y-m-d H:i:s'),
            'event_id' => $id
        ];

        $this->db->insert('registration', $data);
        redirect($redirect_url, 'refresh');
    }
}

/* End of file Form.php */
